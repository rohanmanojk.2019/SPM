INSERT INTO `course` (`courseId`, `courseName`) VALUES ('1', 'Fundamentals of Xerox WorkCenter 7845'), ('2', 'Programming for Xerox WorkCenter with Card Access and Integration');

INSERT INTO `enrollment` (`engineerId`, `classId`, `completed`, `enrolledDate`, `completedDate`, `progress`) VALUES ('1', '101', '1', '2021-10-06', '2021-10-08', '100'), ('2', '101', '0', '2021-10-04', NULL, '50');